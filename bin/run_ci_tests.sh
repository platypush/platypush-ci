#!/bin/bash

BASE_DIR="$(mktemp -d '/tmp/platypush-ci-tests-XXXXX')"
VENV_DIR="$BASE_DIR/venv"
REPO_DIR="/opt/repos/platypush"
TEST_LOG="$BASE_DIR/test.log"

cleanup() {
    echo "Cleaning up environment"
    rm -rf "$BASE_DIR"
}

prepare_venv() {
    echo "Preparing virtual environment"
    python -m venv "$VENV_DIR"
    cd "$VENV_DIR"
    source ./bin/activate
    cd -
}

install_repo() {
    echo "Installing latest version of the repository"
    cd "$REPO_DIR"
    pip install '.[http]'
    cd -
}

run_tests() {
    echo "Running tests"
    cd "$REPO_DIR"
    pytest 2>&1 | tee "$TEST_LOG"
    deactivate

    grep -e '^FAILED ' "$TEST_LOG"
    if [[ $? == 0 ]]; then
        return 2  # FAILURE
    fi

    cd -
    return 0  # PASSED
}

########
# MAIN #
########

cleanup
prepare_venv
install_repo
run_tests
ret=$?
cleanup

if [[ $ret == 0 ]]; then
    echo "Status: PASSED"
else
    echo "Status: FAILED"
fi

exit $ret

