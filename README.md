# Platypush CI

Continuous integration scripts for Platypush.

## Usage

To run the test suite on the latest master branch:

```shell
./bin/run_ci_tests.sh
```

